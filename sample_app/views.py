from django.conf import settings

import braintree
from rest_framework import views, response, status


braintree.Configuration.configure(braintree.Environment.Sandbox,
                                  merchant_id=settings.BT_MERCHANT_ID,
                                  public_key=settings.BT_PUBLIC_KEY,
                                  private_key=settings.BT_PRIVATE_KEY)


class BrainTreeTokenAPIView(views.APIView):
    def get(self, *args, **kwargs):
        try:
            token = braintree.ClientToken.generate()
        except Exception:
            return response.Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        return response.Response({'token': token}, status=status.HTTP_200_OK)
